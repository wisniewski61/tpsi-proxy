import com.sun.net.httpserver.*;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by student on 24.03.2017.
 */
public class HttpProxy {
    private static ArrayList<String> forbiddenList;
    private final static Object syncObject = new Object();

    public static void main(String[] args) throws IOException {
        int port = 8000;
        forbiddenList = new ArrayList<>();

        Files.lines(Paths.get("blacklist.txt")).forEach(f -> forbiddenList.add(f.toString()));
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        RootHandler rootHandler = new RootHandler();
        server.createContext("/", rootHandler);
        System.out.println("Started. Press a key to stop server and save data");
        server.start();
        System.in.read();
        rootHandler.saveStats();
        server.stop(0);
    }

    private static boolean isAllowed(String url) {
        for (String f :
                forbiddenList) {
            if (url.contains(f) || f.contains(url)) return false;
        }//for
        return true;
    }

    static class RootHandler implements HttpHandler {
        private static ArrayList<Stats> statsList;

        public RootHandler() {
            statsList = new ArrayList<>();
        }

        public void saveStats() {
            //List<Stats> countedStats = countStats();
            try{
                PrintWriter writer = new PrintWriter("statistics.csv");
                writer.println("Domain,count,data");
                synchronized (syncObject) {
                    for (Stats s :
                            statsList) {
                        String line = s.DOMAIN + "," + s.COUNT + "," + s.DATA;
                        writer.println(line);
                    }//for
                }//sync
                writer.close();
            } catch (IOException e) {
                // do something
            }
        }

        private void addStats(String domain, long data) {
            synchronized (syncObject) {
                for (Stats s :
                        statsList) {
                    if (s.DOMAIN.equals(domain)) {
                        s.COUNT++;
                        s.DATA += data;
                        return;
                    }//if
                }//for
                Stats s = new Stats();
                s.COUNT = 1;
                s.DATA = data;
                s.DOMAIN = domain;
                statsList.add(s);
            }//synchronized
        }

        public void handle(HttpExchange exchange) throws IOException {
            URI requestURI = exchange.getRequestURI();
            System.out.println(requestURI.toString());

            if(!isAllowed(requestURI.toString())) {
                String response = new String("403");
                exchange.sendResponseHeaders(403, response.getBytes().length);
                OutputStream os = exchange.getResponseBody();
                os.write(response.getBytes());
                os.close();
                addStats(requestURI.getHost(), response.getBytes().length);
                return;
            }//if

            URL url = requestURI.toURL();
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(exchange.getRequestMethod());

            Headers headers = exchange.getRequestHeaders();
            for (Map.Entry<String, List<String>> pair :
                    headers.entrySet()) {
                System.out.println(pair);
                for (String value :
                        pair.getValue()) {
                    //if(!(pair.getKey().equals("Transfer-Encoding") || pair.getKey().equals("Accept-Encoding")))
                        con.addRequestProperty(pair.getKey(), value);
                }//for
            }//for
            InputStream ioreq = exchange.getRequestBody();
            byte[] requestBytes = IOUtils.toByteArray(ioreq);
            if(requestBytes.length>0) {
                try {
                    con.setDoOutput(true);
                    OutputStream conOs = con.getOutputStream();
                    conOs.write(requestBytes);
                    conOs.close();
                }catch (Exception e) {
                    e.printStackTrace();
                }

            }//if

            //con.getHeaderFields().entrySet().addAll(headers.entrySet());
            System.out.println(headers.values());

            //con.setRequestProperty();
            int respCode = con.getResponseCode();
            String respMsg = con.getResponseMessage();
            String respMethod = con.getRequestMethod();

            Map<String, List<String>> responeheaders = con.getHeaderFields();
            for (Map.Entry<String, List<String>> pair :
                    responeheaders.entrySet()) {
                String headerVal = "";
                for(String value: pair.getValue()) {
                    headerVal += value +";";
                }//for
                headerVal = headerVal.substring(0, headerVal.length()-1);
                if(pair.getKey()!=null)
                    exchange.getResponseHeaders().set(pair.getKey(), headerVal);
            }//for
            System.out.println("RespCode: "+respCode);
            System.out.println("RespMsg: "+respMsg);

            exchange.sendResponseHeaders(respCode, 0);

            byte[] contentBytes = new byte[0];
            if(respCode < 400) {
                InputStream iocontent = con.getInputStream();
                contentBytes = IOUtils.toByteArray(iocontent);
            }//if

            OutputStream os = exchange.getResponseBody();
            if(respCode < 400) {
                try {
                    os.write(contentBytes);
                    addStats(requestURI.getHost(), contentBytes.length);
                } catch (IOException e) {
                    addStats(requestURI.getHost(), 0);
                }
            } else
                addStats(requestURI.getHost(), 0);
            os.close();
        }
    }

}
